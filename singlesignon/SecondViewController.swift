//
//  SecondViewController.swift
//  singlesignon
//
//  Created by WILLY KOCHER on 4/4/16.
//  Copyright © 2016 WILLY KOCHER. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let stringUrl = "https://www.instagram.com"
        let url = NSURL(string: stringUrl)
        
        let jar = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        let cookieHeaderField = ["Set-Cookie": "key=value"] // Or ["Set-Cookie": "key=value, key2=value2"] for multiple cookies
        let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(cookieHeaderField, forURL: url!)
        jar.setCookies(cookies, forURL: url!, mainDocumentURL: url!)
        
        // Then
        let request  = NSMutableURLRequest(URL: url!)
        webView.loadRequest(request)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
