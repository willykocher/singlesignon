//
//  FirstViewController.swift
//  singlesignon
//
//  Created by WILLY KOCHER on 4/4/16.
//  Copyright © 2016 WILLY KOCHER. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let stringUrl = "https://www.instagram.com"
        let url = NSURL(string: stringUrl)
        
//        let jar = NSHTTPCookieStorage.sharedHTTPCookieStorage()
//        let cookieHeaderField = ["Set-Cookie": "key=value"] // Or ["Set-Cookie": "key=value, key2=value2"] for multiple cookies
//        let cookies = NSHTTPCookie.cookiesWithResponseHeaderFields(cookieHeaderField, forURL: url!)
//        jar.setCookies(cookies, forURL: url!, mainDocumentURL: url!)
//        
//        // Then
        let request  = NSMutableURLRequest(URL: url!)
        webView.loadRequest(request)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {

        if navigationType == .Reload {
            //logou no instagram agora falta autenticar na API

            let cookies = self.getCookieFromResponse()
            print(cookies)
            return true;
        }
        return false;
    }

    func getCookieFromResponse() -> String? {
        let storage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        let cookies  = storage.cookiesForURL(NSURL(string: "https://www.instagram.com")!)
        var cookieString = ""
        for cookie in cookies!{
            cookieString += "\(cookie.name)=\(cookie.value);"
        }
        return cookieString
    }
}
