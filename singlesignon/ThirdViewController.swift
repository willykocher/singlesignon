//
//  ThirdViewController.swift
//  singlesignon
//
//  Created by WILLY KOCHER on 4/4/16.
//  Copyright © 2016 WILLY KOCHER. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let stringUrl = "https://www.instagram.com"
        let url = NSURL(string: stringUrl)
        
        // Then
        let request  = NSMutableURLRequest(URL: url!)
        webView.loadRequest(request)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
